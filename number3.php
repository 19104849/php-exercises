<!DOCTYPE html>
<html lang="en">
<head>
    <title>PHP Exercises for April 7, 2021</title>
    <style>
    table td{
        background-color: pink;
    }
    table th{
        background-color: violet;
    }
	
    </style>
</head>
<body>
<br>
    <center><h1 style="color:blue;">Number 3</h1>
    <h2 style="color:green">A division table</h2>
<TABLE BORDER=4 style="width:90%">
<?php
$start_num = 1;
$end_num = 10;
  print("<tr>");
  print("<th>/</th>");
  for ($count_1 = $start_num;$count_1 <= $end_num;$count_1++)
    print("<th>$count_1</th>");
  print("</tr>");

  for ($count_1 = $start_num;$count_1 <= $end_num;$count_1++){
    print("<tr><th>$count_1</th>");
    for ($count_2 = $start_num;$count_2 <= $end_num;$count_2++){
        $result = $count_1 / $count_2;
        printf("<td><center>%.1f</center></td>",
               $result);  
      }
    print("</tr>\n");
  }
?> 
</TABLE>
</center>
</body>
</html>

